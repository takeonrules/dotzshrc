#!/bin/bash

# Required parameters:
# @raycast.schemaVersion 1
# @raycast.title Open Emacs
# @raycast.mode compact

# Optional parameters:
# @raycast.icon ⌨
# @raycast.packageName Text Editor

# Documentation:
# @raycast.description Open Emacs
# @raycast.author Jeremy Friesen
# @raycast.authorURL https://jeremyfriesen.com

~/bin/editor ~/git/org/todo.org

exit 0
